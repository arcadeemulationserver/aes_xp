
local xp = "xp"
local ach = "ach"
aes_xp.ach_time = 5

function aes_xp.set_xp(p_name, value)
  aes_xp.player_xp[p_name] = value
  aes_xp.write("player_xp",aes_xp.player_xp)
end

function aes_xp.get_xp(p_name)
  return aes_xp.player_xp[p_name]
end

function aes_xp.sub_xp(p_name, value)
  if value < 0 then value = 0 end
  local mc_value = aes_xp.get_xp(p_name)
  local new_xp = mc_value - value
  if new_xp < 0 then
    new_xp = 0
  end
  aes_xp.set_xp(p_name, new_xp)
end

function aes_xp.add_xp(p_name, value)
  if type(value) == "string" then value = tonumber(value) end
  if value < 0 then value = 0 end
  local mc_value = aes_xp.get_xp(p_name)
  aes_xp.set_xp(p_name, mc_value + value)
end

function aes_xp.change_xp(p_name, value)
  local mc_value = aes_xp.get_xp(p_name)
  local new_xp = mc_value + value
  if new_xp < 0 then
    new_xp = 0
  end
  aes_xp.set_xp(p_name, new_xp)
end

function aes_xp.transfer_xp(from_p_name,to_p_name, value)
  aes_xp.sub_xp(from_p_name, value)
  aes_xp.add_xp(to_p_name, value)
end

function aes_xp.get_level_by_xp(xp)
    if type(xp) == "string" then xp = tonumber(xp) end
    local levels = aes_xp.levels
    local ret_level = 0
    for level, req_xp in ipairs(levels) do
        if xp >= req_xp then
        ret_level = level
        end
    end
    return ret_level
end

function aes_xp.get_level(player)
  
  local xp = aes_xp.get_xp(player)

  if xp ~= nil then
    return aes_xp.get_level_by_xp(xp)
  end
end



-- achievements


function aes_xp.achievement_exists(p_name)
  if aes_xp.achievements[p_name] then
    return true
  end
  return false
end

function aes_xp.save_achievements()
  aes_xp.write("achievements",aes_xp.achievements)
  --minetest.chat_send_all("Achievements saved!")
end

-- creates an achievement, overwrites its properties if it exists, but not the achievers

function aes_xp.add_achievement(ach_name, xp_first_time, xp_after, dec, desc, image, do_custom)
  local achievers
  if aes_xp.achievements[ach_name] then
    achievers = aes_xp.achievements[ach_name].achievers
  end
  aes_xp.achievements[ach_name] = {
    xp_first_time = xp_first_time, -- how much xp can be earned the first time this achievement is unlocked
    xp_after = xp_after, -- how much xp can be earned after the first time
    dec = dec or 0, -- how much to decrease the xp earned each time it is earned after the first
    desc = desc or "", -- a longer description or the achievement
    image = image,
    do_custom = do_custom, -- a function that passes p_name and the achievement table (this table)
    achievers = achievers or {}
  }
  aes_xp.save_achievements()
  
end

-- returns int indicating the number of times a player has achieved an achievement
function aes_xp.player_achieved(p_name,ach_name)
  local ret = 0
  if aes_xp.achievement_exists(ach_name) then
    ret = aes_xp.achievements[ach_name].achievers[p_name] or 0
  end
  return ret
end

-- plays an achievement sound
function aes_xp.play_ach_sound(p_name)
    local ss = "aes_xp_acheievement"
    local sp = {
        to_player = p_name
    }
    minetest.sound_play(ss,sp)
end

-- lets the player know they made an achievement
function aes_xp.hud_achievement(p_name,ach_name,xp,desc,image)

  aes_xp.play_ach_sound(p_name)

  local player = minetest.get_player_by_name(p_name)

  if not player then return end

  local image = image or "aes_xp_controller.png"
  local desc = desc or "Achievement!"

  local idx1 = player:hud_add({
      hud_elem_type = "image",
      text = "aes_xp_ach_bg.png",
      scale = {x = -25, y = -25},
      position = {x = 1, y = 1},
      alignment = {x = -1, y = -1},
      offset = {x = -20, y = -20},
  })
  local idx2 = player:hud_add({
    hud_elem_type = "image",
    text = image,
    scale = {x = 3, y = 3},
    position = {x = .79, y = .875},
    alignment = {x = -1, y = -1},
    offset = {x = 0, y = 0},
  })
  local idx3 = player:hud_add({
    hud_elem_type = "text",
    text = ach_name,
    number = 0xCFC6B8,
    scale = {x = 12.5, y = 3},
    position = {x = .875 , y = .775},
    alignment = {x = 0, y = 0},
    offset = {x = -20, y = -5},
  })
  local idx4 = player:hud_add({
    hud_elem_type = "text",
    text = desc,
    number = 0xA0938E,
    scale = {x = 12.5, y = 3},
    position = {x = .81 , y = .8},
    alignment = {x = 1, y = 1},
    offset = {x = 0, y = 0},
  })


  minetest.after(aes_xp.ach_time,function()
    local player = minetest.get_player_by_name(p_name)
    if not player then return end
    
    player:hud_remove(idx1)
    player:hud_remove(idx2)
    player:hud_remove(idx3)
    player:hud_remove(idx4)
  end)

  minetest.chat_send_player(p_name,minetest.colorize("#a0938e",'[Achievement Unlocked] "'..ach_name..'": You won '..xp..' XP!'))

end





-- plays an levelup sound
function aes_xp.play_level_sound(p_name)
    local ss = "aes_xp_levelup"
    local sp = {
        to_player = p_name
    }
    minetest.sound_play(ss,sp)
end



-- lets the player know they leveled up!
function aes_xp.hud_levelup(p_name)

    minetest.after(aes_xp.ach_time,function()

        local player = minetest.get_player_by_name(p_name)
    
        if not player then return end

        local level = aes_xp.get_level(p_name)

        aes_xp.play_level_sound(p_name)

        local image = image or "aes_xp_controller.png"
    
        local idx1 = player:hud_add({
            hud_elem_type = "image",
            text = "aes_xp_level_bg.png",
            scale = {x = -25, y = -25},
            position = {x = 1, y = 1},
            alignment = {x = -1, y = -1},
            offset = {x = -20, y = -20},
        })
        local idx2 = player:hud_add({
        hud_elem_type = "image",
        text = "aes_xp_levelup.png",
        scale = {x = 1.5, y = 1.5},
        position = {x = .79, y = .875},
        alignment = {x = -1, y = -1},
        offset = {x = 0, y = 0},
        })
        local idx3 = player:hud_add({
        hud_elem_type = "text",
        text = "Level UP!",
        number = 0xCFC6B8,
        scale = {x = 12.5, y = 3},
        position = {x = .875 , y = .775},
        alignment = {x = 0, y = 0},
        offset = {x = -20, y = -5},
        })
        local idx4 = player:hud_add({
        hud_elem_type = "text",
        text = "You Are Now\nLevel "..level,
        number = 0xA0938E,
        scale = {x = 12.5, y = 3},
        position = {x = .81 , y = .8},
        alignment = {x = 1, y = 1},
        offset = {x = 0, y = 0},
        })
    
    
        minetest.after(aes_xp.ach_time,function()
        local player = minetest.get_player_by_name(p_name)
        if not player then return end
        
        player:hud_remove(idx1)
        player:hud_remove(idx2)
        player:hud_remove(idx3)
        player:hud_remove(idx4)
        end)
    
        minetest.chat_send_player(p_name,minetest.colorize("#a0938e",'[Level Up!] You Are Now Level '..level))

    end)

end










-- makes a player achieve an achievement and gain xp
function aes_xp.achieve(p_name,ach_name)
  if aes_xp.achievement_exists(ach_name) then
    local achievement = aes_xp.achievements[ach_name]
    local times = aes_xp.player_achieved(p_name,ach_name) + 1
    local xp = 0
    local oldlevel = aes_xp.get_level(p_name)
    --minetest.chat_send_all(tostring((aes_xp.achievements[ach_name].achievers[p_name] or 0 ) + 1))
    aes_xp.achievements[ach_name].achievers[p_name] = (aes_xp.achievements[ach_name].achievers[p_name] or 0 ) + 1
    aes_xp.save_achievements()
    if times == 1 then
      xp = achievement.xp_first_time
    else
      xp = achievement.xp_after - (times * achievement.dec)
    end
    if xp > 0 then
      aes_xp.hud_achievement(p_name,ach_name,xp,achievement.desc,achievement.image)
      aes_xp.add_xp(p_name,xp)
      local new_level = aes_xp.get_level(p_name)
      if new_level > oldlevel then
        aes_xp.hud_levelup(p_name)
      end
    end
    -- perform the custom function on the player if it exists
    if achievement.do_custom then 
      achievement.do_custom(p_name,aes_xp.achievements[ach_name])
    end
  else return false end
end

--creates an achievement if necassary, overwrites if it it has changed, and lets a player achieve it
function aes_xp.create_and_achieve(p_name, ach_name, xp_first_time, xp_after, dec, desc, image, do_custom)
  aes_xp.add_achievement(ach_name, xp_first_time, xp_after, dec, desc, image, do_custom)
  aes_xp.achieve(p_name,ach_name)
end

-- returns a table indexed by achievement name, with attributes: times, xp_earned, xp_possible
-- times = number of times the player has achieved this, xp_earned is the amt of xp they have earned from this, and xp_possible might be nil if the achievement can be earned an infinite amt of times
-- {
--     ["Myachievement"] = {
--         times = 3,
--         xp_earned = 20,
--         xp_possible = 25
--     }
-- }

function aes_xp.get_player_achievements(p_name)
    
    local len = 0
    local p_ach = {}
    for ach_name,ach in pairs(aes_xp.achievements) do
        len = len + 1
        local times = ach.achievers[p_name] -- how many times the player has achieved this
        if times and times > 0 then
            p_ach[ach_name] = {}
            p_ach[ach_name].times = times
            for i = 1,times do 
                local add = 0
                if times == 1 then
                    add = ach.xp_first_time
                else
                    add = ach.xp_after - (times*ach.dec)
                end
                p_ach[ach_name].xp_earned = (p_ach[ach_name].xp_earned or 0) + add
            end
            if ach.dec and ach.dec > 0 and ach.after ~= 0 then
                local add = ach.xp_first_time
                local times = 1
                while add > 0 do
                    p_ach[ach_name].xp_possible = (p_ach[ach_name].xp_possible or 0) + add
                    times = times + 1
                    add = ach.xp_after - (times*ach.dec)
                end
            end
        end
    end
    return p_ach, len
end



        










