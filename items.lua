
-- controller

-- a table indexed by p_name, values are either 1,2,3 for the page that the player is viewing
aes_xp.xp_menu = {}




function aes_xp.get_ach_page_formspec(p_name)
  local _,ach_len = aes_xp.get_player_achievements(p_name)
  local page_fs = 
    "scrollbaroptions[max="..tostring(50 * ach_len)..";thumbsize="..tostring(ach_len)..";]"..
    "scrollbar[20.5,2;.8,12;vertical;mainscroll;0]"..
    "scroll_container[.5,2;19,12;mainscroll;vertical;]"
  local y = 0
  for ach_name,ach_info in pairs(aes_xp.get_player_achievements(p_name)) do
    local achdesc = aes_xp.achievements[ach_name].desc or ""
    local image = aes_xp.achievements[ach_name].image or "aes_xp_controller.png"
    local xp_str = tostring(ach_info.xp_earned)
    if ach_info.xp_possible then
      xp_str = xp_str .. "/"..tostring(ach_info.xp_possible)
    end
    page_fs = page_fs .. 
    
    "container[1,"..y.."]"..
    
    "image[5,1;5,5;aes_xp_gui_bg.png^[colorize:#5a5353:255]"..
    "image[9,1;5,5;aes_xp_gui_bg.png^[colorize:#5a5353:255]"..
    "image[13,1;5,5;aes_xp_gui_bg.png^[colorize:#5a5353:255]"..
    "image[1,1;5,5;aes_xp_gui_bg.png^[colorize:#7d7071:255]"..
    
    "image[2,2;3,3;"..image.."]"..
    "label[7,2;"..ach_name.."]"..
    "label[7.5,3;Earned "..ach_info.times.."x, XP: "..xp_str.."]"..
    "textarea[7.5,3.6;9.5,2;;"..achdesc..";]"..
    "container_end[]"
    y = y + 5.5
  end
  page_fs = page_fs .. "scroll_container_end[]"
  return page_fs

end


aes_xp.get_player_controller_fs = function(p_name)


  if not aes_xp.xp_menu[p_name] then aes_xp.xp_menu[p_name] = 1 end

  local page_fs = ""
  
  -- xp and level page

  if aes_xp.xp_menu[p_name] == 1 then

    page_fs = 
    "image[4,4;4,4;aes_xp_xp.png]"..
    "hypertext[4,9;4,5;xptext;<style size=30>"..aes_xp.get_xp(p_name).."</style>]"..
    
    "image[14,4;4,4;aes_xp_levelup.png]"..
    "hypertext[14,9;4,4;lvltext;<style size=30>"..aes_xp.get_level(p_name).."</style>]"

  end

  -- achievements page

  if aes_xp.xp_menu[p_name] == 2 then
    page_fs =  aes_xp.get_ach_page_formspec(p_name)
  end
    
    
  if aes_xp.xp_menu[p_name] == 3 then
    -- store/bling page (not implemented yet)
  end
    



  local fs = "formspec_version[4]"..
  "size[25,25]"..
  "position[0.5,0.5]"..
  "no_prepend[]"..
  "bgcolor[#00000000;neither]"..
  "background[0,0;25,25;aes_xp_gui_bg.png^[colorize:#472d3c:255]"..
  "container[1,1]"..
  
  "image[1,1;8,8;aes_xp_gui_bg.png^[colorize:#5e3643:255]"..
  "image[7,1;8,8;aes_xp_gui_bg.png^[colorize:#5e3643:255]"..
  "image[14,1;8,8;aes_xp_gui_bg.png^[colorize:#5e3643:255]"..
  "image[1,7;8,8;aes_xp_gui_bg.png^[colorize:#5e3643:255]"..
  "image[7,7;8,8;aes_xp_gui_bg.png^[colorize:#5e3643:255]"..
  "image[14,7;8,8;aes_xp_gui_bg.png^[colorize:#5e3643:255]"..
  
  "image_button[2,17;4,4;aes_xp_xp.png;levels;;;false;]"..
  "image_button[9.5,17;4,4;aes_xp_controller.png;ach;;;false;]"..
  "image_button[17,17;4,4;aes_xp_store.png;store;;;false;]"..
  
  page_fs..
  
  "container_end[]"
  return fs
end


minetest.register_on_player_receive_fields(function(player, formname, fields)
  
  if formname ~= "aes_xp:controller" then return end
  local p_name = player:get_player_name()
  if fields.levels then
    aes_xp.xp_menu[p_name] = 1
    minetest.show_formspec(player:get_player_name(), "aes_xp:controller",aes_xp.get_player_controller_fs(p_name))
  end
  if fields.ach then
    aes_xp.xp_menu[p_name] = 2
    minetest.show_formspec(player:get_player_name(), "aes_xp:controller",aes_xp.get_player_controller_fs(p_name))
  end
  if fields.store then
    aes_xp.xp_menu[p_name] = 3
    minetest.show_formspec(player:get_player_name(), "aes_xp:controller",aes_xp.get_player_controller_fs(p_name))
  end
end)



minetest.register_craftitem("aes_xp:controller", {
  inventory_image = "aes_xp_controller.png",
  description = "XP and Achievments",
  on_use = function(itemstack, player, pointed_thing)
    local p_name = player:get_player_name()
    aes_xp.xp_menu[p_name] = 1
    minetest.show_formspec(player:get_player_name(), "aes_xp:controller",aes_xp.get_player_controller_fs(p_name))
  
  end,
})