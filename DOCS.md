
api
==============

You can register new achievements with the following code:

```lua
aes_xp.add_achievement(ach_name, xp_first, xp_after, xp_dec, desc, img, do_custom(p_name,ach_name))
```

where ach_name is the Title of the achievement,
xp_first is the amount of xp the player recieves the first time they achieve it
xp_after is the baseline amount of xp they should recieve after the first time
xp_dec is the amount to decrease the reward by each time they achieve it (can be 0)
desc is the description of the achievement
img is the image to use (defaults to a prize cup)
do_custom is a function that passes p_name and ach_name to do whatever you like when an achievement is gotten (untested)

you can give a player an achievement with:
```lua
aes_xp.achieve(p_name,ach_name)
```


commands
==============

the base command is /xp

    - add <player> <value>  -- add xp to a player's account
    - sub <player> <value>  -- remove xp from a player's account
    - get <player>          -- prints a player's XP
    - set <player> <value>  -- sets a player's XP
    - achievement <achievement_name> <player>  -- makes a player get an achievement
    - achievement_create <achievement_name> <first_xp> <after_xp> <dec> <img> <description>
    -- creates a new achievement or overwrites an existing achievement, 
    img may be a image string or nil or none
    in achievement_name, use underscores for spaces