
-- chatcommands


ChatCmdBuilder.new("xp", function(cmd)

	cmd:sub("add :player :value", function(name, target, value)
		aes_xp.add_xp(target, value)
    minetest.chat_send_player(target, name .. " gave you: ".. value .." XP!")
    minetest.chat_send_player(target, "You gave" ..target .." ".. value .." XP!")
  end)

  cmd:sub("sub :player :value", function(name, target, value)
		aes_xp.sub_xp(target, value)
    minetest.chat_send_player(target, name .. " removed ".. value .." XP.")
    minetest.chat_send_player(target, "You removed" .. value .." XP from "..target)
  end)

  cmd:sub("get :player", function(name, target)
		local player = minetest.get_player_by_name(target)
		local xp = aes_xp.get_xp(target)
    minetest.chat_send_player(name, target .. " has "..xp.." XP.")
  end)

  cmd:sub("set :player :value", function(name, target, value)
		local player = minetest.get_player_by_name(target)

		aes_xp.set_xp(target, value)
    minetest.chat_send_player(target, name .. " set your XP to ".. value)
    minetest.chat_send_player(target,  target .. "'s xp has been set to ".. value)
  end)

  cmd:sub("achievement :achname :pname", function(name, ach_name, p_name)
    ach_name = ach_name:gsub("%_", " ")
    aes_xp.achieve(p_name,ach_name)
  end)

  cmd:sub("achievementcreate :achname:alphascore :xpfirst:int :xpafter:int :xpdec:int :img :desc:text", function(name, ach_name, xp_first, xp_after, xp_dec,img,desc)
    ach_name = ach_name:gsub("%_", " ")
    if img == "nil" or img == "none" then
      img = nil
    end
    aes_xp.add_achievement(ach_name, xp_first, xp_after, xp_dec, desc, img)
  end)



end, {
	description = [[

    (/help xp)

    - add <player> <value>  -- add xp to a player's account
    - sub <player> <value>  -- remove xp from a player's account
    - get <player>          -- prints a player's XP
    - set <player> <value>  -- sets a player's XP
    - achievement <achievement_name> <player>  -- makes a player get an achievement
    - achievement_create <achievement_name> <first_xp> <after_xp> <dec> <img> <description>
    -- creates a new achievement or overwrites an existing achievement, img may be a image string or nil or none
    ]],
	privs = {
		aes_xp_admin = true,
	}
})
