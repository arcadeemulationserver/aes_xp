aes_xp = {}

-- storage
local s = minetest.get_mod_storage()

function aes_xp.write(key,value)
  s:set_string(key, minetest.serialize(value))
end
function aes_xp.read(key)
  return minetest.deserialize(s:get_string(key))
end

--global persistant tables
aes_xp.player_xp = aes_xp.read("player_xp") or {}
aes_xp.player_ach = aes_xp.read("player_ach") or {}
aes_xp.achievements = aes_xp.read("achievements") or {}

if not minetest.get_modpath("lib_chatcmdbuilder") then
    dofile(minetest.get_modpath("aes_xp") .. "/chatcmdbuilder.lua")
end


minetest.register_privilege("aes_xp_admin", "Needed for xp things")

aes_xp.levels = { 
  125,
  250,
  500,
  1000,
  2000,
  3000,
  5000,
  7500,
  10000,
  12500,
  15000,
  20000,
  25000,
  30000,
  40000,
  50000,
  60000,
  75000,
  90000,
  110000,
  140000,
}


local path = minetest.get_modpath("aes_xp")

dofile(path.."/api.lua")
dofile(path.."/joinplayer.lua")
dofile(path.."/items.lua")
dofile(path.."/commands.lua")

